import os
import glob
import json
import sys

from tqdm import tqdm_notebook as tqdm
import numpy as np
import pandas as pd
import cv2
import imutils
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
# import tensorflow as tf

sys.path.append('../src')
from utils import get_iou, get_max_iou

from hyperopt import fmin, hp, tpe, Trials
from hyperopt import SparkTrials, STATUS_OK


def red_detector(input, h1, h2, s_low, v_low, s_high, v_high):
    ret1 = cv2.inRange(input, (0,s_low,v_low), (h1,s_high,v_high))
    ret2 = cv2.inRange(input, (h2,s_low,v_low), (180,s_high,v_high))
    return cv2.bitwise_or(ret1, ret2)
#     return ret2

def blue_detector(input, h1, h2, s_low, v_low, s_high, v_high):
    ret = cv2.inRange(input, (h1,s_low,v_low), (h2,s_high,v_high))
    return ret

def yellow_detector(input, h1, h2, s_low, v_low, s_high, v_high):
    ret = cv2.inRange(input, (h1,s_low,v_low), (h2,s_high,v_high))
    return ret

def filter_boxes(img, x, y, w, h, min_size_ratio=0.005, min_w_h_ratio=0.7, min_h_w_ratio=0.7):
    H, W, _ = img.shape
    
    if(w < min_size_ratio*W or h < min_size_ratio*H):
        return False
    if(w/h < min_w_h_ratio or h/w < min_h_w_ratio):
        return False
    
    return True

def extract_cropped_images(img, mask):
    candidates = []
    bboxes = []

    #find contour with privided mask
    cnts, hierarchy = cv2.findContours(mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

    for cnt in cnts:
        x,y,w,h = cv2.boundingRect(cnt)

        if(not filter_boxes(img, x, y, w, h)):
            continue

        cropped = img[y:y+w+1,x:x+h+1]
        candidates.append(cropped)
        bboxes.append([x,y,x+w,y+h])
        
    return candidates, bboxes
        
def extract_candidate(image, dectector_funct, h1, h2, s_low, v_low, s_high, v_high):
    hsv = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
    mask = dectector_funct(hsv, h1, h2, s_low, v_low, s_high, v_high)
#     cv2.imshow("mask", blue_mask)
    candidates, bboxes = extract_cropped_images(image, mask)
    return bboxes, candidates

def detect_bboxes(img):
    bboxes_blue, cropped_images_blue = extract_candidate(img, blue_detector, 76, 120, 69, 60, 249, 252)
    bboxes_red, cropped_images_red = extract_candidate(img, red_detector, 10, 156, 78, 54, 213, 210)
    bboxes_yellow, cropped_images_yellow = extract_candidate(img, yellow_detector, 10, 156, 78, 54, 213, 210)
    
    bboxes = bboxes_blue+bboxes_red+bboxes_yellow
    cropped_images = cropped_images_blue + cropped_images_red + cropped_images_yellow
   
    return bboxes, cropped_images

def compute_detection_recall_loss(annotations, image_folder, verbose=False):
    c_match = 0
    c_not_match = 0

    if(verbose):
        ann_iter = tqdm(annotations.iterrows())
    else:
        ann_iter = annotations.iterrows()

    for i, row in ann_iter:
        path = os.path.join(image_folder, row['frame'])
        img = cv2.imread(path)
        
        bboxes, cropped_images = detect_bboxes(img)

        if(len(bboxes)==0):
            c_not_match += 1
            continue

        gt_boxes = [row['xmin'], row['ymin'], row['xmax'], row['ymax']]

        try:
            ious, max_ious, index_max = get_max_iou(np.array(bboxes), np.array(gt_boxes))
        except:
            print(bboxes)
        if(max_ious >= 0.5):
            b = bboxes[index_max]

            cv2.rectangle(img, (b[0], b[1]), (b[2], b[3]), (0,255,0), 2)

            c_match += 1
        else:
            c_not_match +=1
    
    loss = c_not_match / (c_match+c_not_match)
    
    return loss
