
import os
import glob
import json

import random
from tqdm import tqdm
import pandas as pd
import cv2
import imutils
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

import tensorflow as tf
from tensorflow.keras.preprocessing.image import ImageDataGenerator
from tensorflow.keras import models, layers, losses, optimizers

LABEL_CLASSES = [None, {'supercategory': 'Cấm ngược chiều', 'id': 1, 'name': 'Cấm ngược chiều'},
 {'supercategory': 'Cấm dừng và đỗ', 'id': 2, 'name': 'Cấm dừng và đỗ'},
 {'supercategory': 'Cấm rẽ', 'id': 3, 'name': 'Cấm rẽ'},
 {'supercategory': 'Giới hạn tốc độ', 'id': 4, 'name': 'Giới hạn tốc độ'},
 {'supercategory': 'Cấm còn lại', 'id': 5, 'name': 'Cấm còn lại'},
 {'supercategory': 'Nguy hiểm', 'id': 6, 'name': 'Nguy hiểm'},
 {'supercategory': 'Hiệu lệnh', 'id': 7, 'name': 'Hiệu lệnh'}]

CONFIG = {
    'W':64, 'H':64, 'n_pos_classes': 7
}
CONFIG['n_classes'] = CONFIG['n_pos_classes'] + 1

def batch_preprocess(list_imgs):
    ret_image_batches = []
    for img in list_imgs:
        processed = cv2.resize(img, (CONFIG['W'], CONFIG['H']))
        processed = cv2.cvtColor(processed, cv2.COLOR_BGR2RGB)
        processed = processed.astype(float)
        processed /= 255.0
        ret_image_batches.append(processed)
        
    return np.array(ret_image_batches)

def classify_images(model, cropped_images):
    batch_cropped_imgs = batch_preprocess(cropped_images)
    probs = model.predict(batch_cropped_imgs)
    class_predictions = np.argmax(probs, axis=1)

    return np.max(probs, axis=1), class_predictions
    